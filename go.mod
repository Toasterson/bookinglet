module gitlab.com/toasterson/bookinglet

go 1.13

require (
	github.com/99designs/gqlgen v0.10.2
	github.com/gin-gonic/gin v1.5.0
	github.com/jinzhu/gorm v1.9.12
	github.com/lib/pq v1.3.0 // indirect
	github.com/pkg/errors v0.8.1
	github.com/satori/go.uuid v1.2.0
	github.com/spf13/viper v1.6.2
	github.com/vektah/gqlparser v1.2.0
)
