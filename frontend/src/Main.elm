module Main exposing (Model, init, Msg, update, view, subscriptions)

import Css
import Browser
import Browser.Navigation as Nav
import Html.Styled.Attributes as HtmlAttrs
import Html.Styled as HtmlStyled
import Url
import Url.Parser exposing (Parser, (</>))


main : Program () Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = UrlRequested
        , onUrlChange = UrlChanged
    }


type alias Model =
    { key : Nav.Key
    , url : Url.Url
    }


init : () -> Url.Url -> Nav.Key -> (Model, Cmd Msg)
init _ url key =
    ({key=key, url=url}, Cmd.none)


type Msg
    = UrlRequested Browser.UrlRequest
    | UrlChanged Url.Url


update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of

        UrlRequested urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.key (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        UrlChanged url ->
            ({url = url, key = model.key}, Cmd.none)


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


type Route
  = AccountList
  | Account String
  | Holder String
  | NotFound

routeParser : Parser (Route -> a) a
routeParser =
  Url.Parser.oneOf
    [ Url.Parser.map AccountList Url.Parser.top
    , Url.Parser.map Account    (Url.Parser.s "account" </> Url.Parser.string)
    , Url.Parser.map Holder    (Url.Parser.s "holder" </> Url.Parser.string)
    ]

fromUrl : Url.Url -> Route
fromUrl url =
    Maybe.withDefault NotFound (Url.Parser.parse routeParser url)

view : Model -> Browser.Document Msg
view model =
    let body = HtmlStyled.div [ HtmlAttrs.id "app", HtmlAttrs.css [
                       Css.batch [
                           Css.property "display" "grid"
                           , Css.property "grid-template-areas" ("\"header header header\""  ++ "\"sidebar content content\"" ++ "\"sidebar content content\"" )
                           , Css.property "height" "100vh"
                           , Css.property "grid-template-rows" "100px 1fr 1fr"
                           , Css.property "grid-template-columns" "400px 1fr 1fr"
                       ]] ] [
                            HtmlStyled.div [HtmlAttrs.id "header", HtmlAttrs.css [
                                Css.batch [
                                    Css.property "grid-area" "header"
                                    , Css.backgroundColor (Css.hex "#743287")
                                ]
                            ]] [

                            ]
                            , HtmlStyled.div [HtmlAttrs.id "sidebar", HtmlAttrs.css [
                                  Css.batch [
                                      Css.property "grid-area" "sidebar"
                                      , Css.backgroundColor (Css.hex "#22243e")
                                      , Css.color (Css.hex "#f5f5f5;")
                                  ]
                              ]] [
                                HtmlStyled.div [ HtmlAttrs.css [
                                    Css.padding (Css.px 7)
                                    , Css.height (Css.pct 100)
                                ]] [
                                    HtmlStyled.div [HtmlAttrs.css [
                                        Css.backgroundColor (Css.hex "#ffff")
                                        , Css.width (Css.pct 100)
                                        , Css.height (Css.px 400)
                                        , Css.margin (Css.px 0)
                                        , Css.borderRadius (Css.px 5)
                                    ]] [
                                        HtmlStyled.input [HtmlAttrs.css [
                                            Css.width (Css.pct 100)
                                            , Css.padding (Css.px 0)
                                            , Css.boxSizing Css.borderBox
                                            , Css.borderTopLeftRadius (Css.px 5)
                                            , Css.borderTopRightRadius (Css.px 5)
                                        ], HtmlAttrs.placeholder "Search for Account" ] []
                                    ]
                                ]
                              ]
                            , HtmlStyled.div [HtmlAttrs.id "content", HtmlAttrs.css [
                                  Css.batch [
                                      Css.property "grid-area" "content"
                                  ]
                              ]] []
                       ]
    in
  { title = "Bookinglet"
  , body = [ HtmlStyled.toUnstyled body ]
  }


viewLink : String -> HtmlStyled.Html msg
viewLink path =
  HtmlStyled.li [] [ HtmlStyled.a [ HtmlAttrs.href path ] [ HtmlStyled.text path ] ]


