package main

//go:generate go run github.com/99designs/gqlgen -v

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"github.com/99designs/gqlgen/graphql"
	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
) // THIS CODE IS A STARTING POINT ONLY. IT WILL NOT BE UPDATED WITH SCHEMA CHANGES.

type Resolver struct{}

func (r *Resolver) Account() AccountResolver {
	return &accountResolver{r}
}

func (r *Resolver) AccountHolder() AccountHolderResolver {
	return &accountHolderResolver{r}
}

func (r *Resolver) Booking() BookingResolver {
	return &bookingResolver{r}
}

func (r *Resolver) Mutation() MutationResolver {
	return &mutationResolver{r}
}

func (r *Resolver) Query() QueryResolver {
	return &queryResolver{r}
}

type accountResolver struct{ *Resolver }

func (r *accountResolver) Iban(ctx context.Context, obj *Account) (*string, error) {
	s := ""
	if obj.Iban != "" {
		s = string(obj.Iban)
	}
	return &s, nil
}

func (r *accountResolver) Holder(ctx context.Context, obj *Account) (*AccountHolder, error) {
	var h AccountHolder
	if err := db.First(&h, "id = ?", obj.Holder).Error; err != nil {
		graphql.AddErrorf(ctx, "could not find holder %s: %e", obj.Holder, err)
		return nil, nil
	}
	return &h, nil
}

type accountHolderResolver struct{ *Resolver }

func (r *accountHolderResolver) Fistname(ctx context.Context, obj *AccountHolder) (*string, error) {
	if obj.Fistname.Valid {
		return &obj.Fistname.String, nil
	}
	return nil, nil
}

func (r *accountHolderResolver) Lastname(ctx context.Context, obj *AccountHolder) (*string, error) {
	if obj.Lastname.Valid {
		return &obj.Lastname.String, nil
	}
	return nil, nil
}

type bookingResolver struct{ *Resolver }

func (r *bookingResolver) Account(ctx context.Context, obj *Booking) (*Account, error) {
	var account Account
	if err := db.First(&account, "iban = ?", obj.Account).Error; err != nil {
		graphql.AddErrorf(ctx, "booking %s: %s %s -> %s not consistent please check", obj.Timestamp.Format(time.RFC3339), obj.Note, obj.Account, obj.ForeignAccount)
		return nil, nil
	}
	return &account, nil
}

func (r *bookingResolver) ForeignAccount(ctx context.Context, obj *Booking) (*Account, error) {
	var account Account
	if err := db.First(&account, "iban = ?", obj.ForeignAccount).Error; err != nil {
		graphql.AddErrorf(ctx, "booking %s: %s %s -> %s not consistent please check", obj.Timestamp.Format(time.RFC3339), obj.Note, obj.Account, obj.ForeignAccount)
		return nil, nil
	}
	return &account, nil
}

func (r *bookingResolver) Timestamp(ctx context.Context, obj *Booking) (string, error) {
	return obj.Timestamp.Format(time.RFC3339), nil
}

type mutationResolver struct{ *Resolver }

func (r *mutationResolver) AddBooking(ctx context.Context, account NewBooking) (*Booking, error) {

	amountInt, err := strconv.Atoi(account.Amount.Amount)
	if err != nil {
		return nil, errors.Wrap(err, "addBooking")
	}

	b := Booking{
		ID:             uuid.NewV4().String(),
		Timestamp:      account.Timestamp,
		Note:           account.Note,
		Account:        IBAN(account.Iban),
		ForeignAccount: IBAN(account.ForeignIban),
		Amount: Money{
			Amount:   amountInt,
			Currency: account.Amount.Currency,
		},
		Tags: make([]Tag, 0),
	}

	var acc Account
	acc.Iban = b.Account
	if err := db.Where("iban = ?", acc.Iban).FirstOrCreate(&acc).Error; err != nil {
		graphql.AddErrorf(ctx, "Account %s could not be crated adding booking anyway: %e", b.Account, err)
	}

	var facc Account
	facc.Iban = b.ForeignAccount
	if err := db.Where("iban = ?", facc.Iban).FirstOrCreate(&facc).Error; err != nil {
		graphql.AddErrorf(ctx, "Account %s could not be crated adding booking anyway: %e", b.ForeignAccount, err)
	}

	for _, ti := range account.Tags {
		b.Tags = append(b.Tags, Tag{Name: ti})
	}

	if err := db.Model(&b).Save(&b).Error; err != nil {
		graphql.AddErrorf(ctx, "could not save booking %s at %s %s: %e", b.Account, b.Timestamp, b.Note, err)
		return nil, nil
	}

	//if err := db.Model(&b).Association("Tags").Append(&b.Tags).Error; err != nil {
	//	graphql.AddErrorf(ctx, "could not save tags for booking %s at %s %s: %e", b.Account, b.Timestamp, b.Note, err)
	//	return nil, nil
	//}
	return &b, nil
}

func (r *mutationResolver) IdentifyAccount(ctx context.Context, ident IdentifyInput) (*Account, error) {
	var acc Account
	h := *ident.Holder

	if err := db.FirstOrInit(&acc, "iban = ?", ident.Iban).Error; err != nil {
		return nil, fmt.Errorf("could identify account %s: %e", ident.Iban, err)
	}

	acc.Name = ident.Name
	if ident.AccountNumber != nil {
		acc.AccountNumber = *ident.AccountNumber
	}

	if err := db.Model(&h).FirstOrInit(&h).Error; err != nil {
		return nil, fmt.Errorf("could not find or init account holder %s: %e", ident.Holder.Name, err)
	}

	if err := db.FirstOrCreate(&acc).Related(&h).Error; err != nil {
		return nil, fmt.Errorf("could not find or create account for identification %s: %e", acc.Name, err)
	}

	return &acc, nil
}

type queryResolver struct{ *Resolver }

func (r *queryResolver) GetBookings(ctx context.Context, ids []*string, startTime *string, endTime *string) ([]*Booking, error) {
	bookings := make([]Booking, 0)
	if ids != nil {
		if err := db.Where(ids).Preload("Tags").Find(&bookings).Error; err != nil {
			return nil, fmt.Errorf("could not retrieve bookings %e", err)
		}
	} else {
		if startTime == nil {
			return nil, fmt.Errorf("either provide a list of ids or at least a startTime")
		}

		startTimeT, err := time.Parse(time.RFC3339, *startTime)
		if err != nil {
			return nil, errors.Wrap(err, "getBooking")
		}

		var endTimeT time.Time

		if endTime == nil {
			endTimeT = time.Now()
		} else {
			startTimeT, err = time.Parse(time.RFC3339, *startTime)
			if err != nil {
				return nil, errors.Wrap(err, "getBooking")
			}
		}

		if err := db.Where("timestamp > ? AND timestamp < ?", startTimeT, endTimeT).Preload("Tags").Find(&bookings).Error; err != nil {
			return nil, fmt.Errorf("could not retrieve bookings by time: %e", err)
		}
	}

	res := make([]*Booking, len(bookings))
	for i, r := range bookings {
		res[i] = &r
	}
	return res, nil
}

func (r *queryResolver) GetAccounts(ctx context.Context, ibans []string) ([]*Account, error) {
	accounts := make([]Account, 0)
	if err := db.Where("iban IN (?)", ibans).Find(&accounts).Error; err != nil {
		return nil, fmt.Errorf("could not retrieve accounts: %e", err)
	}
	res := make([]*Account, len(accounts))
	for i, a := range accounts {
		res[i] = &a
	}
	return res, nil
}

func (r *queryResolver) GetAccountDetails(ctx context.Context, iban *string, accountNumber *string) (*Account, error) {
	var account Account
	if iban != nil {
		if err := db.First(&account, "iban = ?", iban).Error; err != nil {
			return nil, fmt.Errorf("could not read account %s from db: %e", *iban, err)
		}
		return &account, nil
	}

	if err := db.First(&account, "account_number = ?", accountNumber).Error; err != nil {
		return nil, fmt.Errorf("could not read account %s from db: %e", *accountNumber, err)
	}

	if account.Name == "" {
		return nil, fmt.Errorf("account %s/%s could not be found in db", *iban, *accountNumber)
	}

	return &account, nil
}

func (r *queryResolver) GetHolders(ctx context.Context, ids []string) ([]*AccountHolder, error) {
	holders := make([]AccountHolder, 0)
	if err := db.Where(ids).Find(&holders).Error; err != nil {
		return nil, fmt.Errorf("could not load any holders from db: %e", err)
	}
	res := make([]*AccountHolder, len(holders))
	for i, h := range holders {
		res[i] = &h
	}
	return res, nil
}

func (r *mutationResolver) Tag(ctx context.Context, bookingID string, tags []string) (*Booking, error) {
	var booking Booking
	booking.ID = bookingID
	t := make([]Tag, len(tags))
	for i, ti := range tags {
		var tag Tag
		tag.Name = ti
		if err := db.Where("name = ?", ti).FirstOrCreate(&tag).Error; err != nil {
			return nil, fmt.Errorf("could not find or create tag %s: %e", ti, err)
		}

		t[i] = tag
	}

	if err := db.Model(&booking).Association("Tags").Append(t).Error; err != nil {
		return nil, fmt.Errorf("could not append tags to booking with id %s: %e", bookingID, err)
	}

	if err := db.Where("id = ?", bookingID).First(&booking).Error; err != nil {
		return nil, fmt.Errorf("could not load booking %s, %e", bookingID, err)
	}

	return &booking, nil
}

func (r *mutationResolver) Untag(ctx context.Context, bookingID string, tags []string) (*Booking, error) {
	var booking Booking
	if err := db.Model(&booking).Where("id = ?", bookingID).Association("Tags").Delete(tags).Find(&booking).Error; err != nil {
		return nil, fmt.Errorf("could not append tags to booking with id %s: %e", bookingID, err)
	}
	return &booking, nil
}
