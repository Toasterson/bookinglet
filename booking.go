package main

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"reflect"
	"time"

	"github.com/jinzhu/gorm"
)

type Booking struct {
	ID             string    `json:"id" gorm:"primary_key"`
	Timestamp      time.Time `json:"timestamp"`
	Note           string    `json:"note"`
	Account        IBAN      `json:"Account"`
	ForeignAccount IBAN      `json:"foreignAccount"`
	Amount         Money     `json:"amount" gorm:"type:jsonb"`
	Tags           []Tag     `json:"tags" gorm:"many2many:bookings_tags"`
}

type Tag struct {
	gorm.Model
	Name string `json:"name" gorm:"unique"`
}

const (
	CurrencyCHF = "CHF"
	CurrencyEUR = "EUR"
)

type Money struct {
	Negative bool   `json:"negative"`
	Amount   int    `json:"amount"`
	Currency string `json:"currency"`
}

func (m *Money) Scan(src interface{}) error {
	switch src.(type) {
	case []byte:
		return json.Unmarshal(src.([]byte), m)
	case nil:
		return nil
	}
	return fmt.Errorf("type %s is not supported as money type", reflect.TypeOf(src).String())
}

func (m Money) Value() (driver.Value, error) {
	return json.Marshal(&m)
}

func (m Money) String() string {
	fAmount := float64(m.Amount) / 100
	if m.Negative {
		return fmt.Sprintf("- %2f %s", fAmount, m.Currency)
	}
	return fmt.Sprintf("+ %2f %s", fAmount, m.Currency)
}
