#!/usr/bin/env bash

export POSTGRES_USER="bookinglet"
export POSTGRES_PASSWORD="bookinglet"
export POSTGRES_HOST="localhost"
export POSTGRES_PORT="5432"
export POSTGRES_DB="bookings"
export DEBUG="true"

go run ./
