package main

import (
	"context"
	"fmt"
	"log"

	"github.com/99designs/gqlgen/handler"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/spf13/viper"
)

const defaultPort = ":8080"

var db *gorm.DB

func buildConnectionString() string {
	user := viper.GetString("POSTGRES_USER")
	pass := viper.GetString("POSTGRES_PASSWORD")
	if user == "" || pass == "" {
		log.Fatalln("You must include POSTGRES_USER and POSTGRES_PASSWORD environment variables")
	}
	host := viper.GetString("POSTGRES_HOST")
	port := viper.GetString("POSTGRES_PORT")
	dbname := viper.GetString("POSTGRES_DB")
	if host == "" || port == "" || dbname == "" {
		log.Fatalln("You must include POSTGRES_HOST, POSTGRES_PORT, and POSTGRES_DB environment variables")
	}

	return fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable", user, pass, host, port, dbname)
}

func main() {
	viper.SetDefault("DEBUG", false)
	viper.AutomaticEnv()
	port := viper.GetString("PORT")
	if port == "" {
		port = defaultPort
	}

	var err error
	if db, err = gorm.Open("postgres", buildConnectionString()); err != nil {
		panic(err)
	}

	if viper.GetBool("DEBUG") {
		gin.SetMode("debug")
	} else {
		gin.SetMode("release")
	}

	if gin.IsDebugging() {
		db.LogMode(true)
	}

	if err := db.AutoMigrate(Account{}, AccountHolder{}, Booking{}, Tag{}).Error; err != nil {
		panic(err)
	}

	r := gin.Default()
	//r.Use(func(context *gin.Context) {
	//context.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	//})
	r.Use(GinContextToContextMiddleware())
	r.POST("/query", graphqlHandler())
	r.GET("/", playgroundHandler())
	if err := r.Run(port); err != nil {
		panic(err)
	}
}

func GinContextToContextMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := context.WithValue(c.Request.Context(), "GinContextKey", c)
		c.Request = c.Request.WithContext(ctx)
		c.Next()
	}
}

func graphqlHandler() gin.HandlerFunc {
	h := handler.GraphQL(NewExecutableSchema(Config{Resolvers: &Resolver{}}))
	return func(context *gin.Context) {
		h.ServeHTTP(context.Writer, context.Request)
	}
}

func playgroundHandler() gin.HandlerFunc {
	h := handler.Playground("GraphQL", "/query")

	return func(c *gin.Context) {
		h.ServeHTTP(c.Writer, c.Request)
	}
}
