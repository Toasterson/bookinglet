package main

import "database/sql"

type IBAN string

func (I IBAN) String() string {
	return string(I)
}

type Account struct {
	Iban          IBAN          `json:"iban" gorm:"primary_key"`
	AccountNumber string        `json:"accountNumber" gorm:"unique"`
	Holder        AccountHolder `json:"holder" gorm:"foreignkey:HolderID"`
	HolderID      string        `json:"-"`
	Name          string        `json:"name"`
}

type AccountHolder struct {
	ID       string         `json:"id"`
	Name     string         `json:"name"`
	Fistname sql.NullString `json:"fistname"`
	Lastname sql.NullString `json:"lastname"`
}
